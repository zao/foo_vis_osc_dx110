struct VSInput {
	int2 vertex_id : VERTEX_ID;
	float2 from : POINT0;
	float2 to   : POINT1;
	float width : WIDTH;
	float r     : R;
};

struct PSInput {
	float4 pos      : SV_Position;
	float2 p        : REL_P;
	float2 p1       : REL_P1;
	float2 major    : MAJOR_AXIS;
	float inv_width : INV_WIDTH;
	float p1_len    : LEN_P1;
};

cbuffer matrix_buffer
{
    float2 window_extents;
};

PSInput main(VSInput input) {
	PSInput output;
	uint vtx_id = (uint)abs(input.vertex_id.x);
	float x_id = (vtx_id%2);
    float y_id = (vtx_id/2);

    float w = input.width;
	float R = input.r;
    float2 mid = (input.to + input.from)/2;
    float2 A = input.to - input.from;
    float A_len = length(A);
    A = A/A_len;
    float2 B = float2(A.y, -A.x);

    float2 major = A*(R + w + A_len/2);
    float2 minor = B*(R + w);

    float2 v = mid 
        + (x_id*2 - 1.0) * major
        - (y_id*2 - 1.0) * minor;
//    v = mid 
//        + (x_id*2 - 1.0) * float2(window_extents.x, 0)
//        - (y_id*2 - 1.0) * float2(0, window_extents.y);
	output.pos = float4(2.0 * v / window_extents - 1.0, 0.5, 1.0);
	output.p = v - input.from;
	output.p1 = input.to - input.from;
	output.major = A;
	output.inv_width = 2/(input.width + input.r);
	output.p1_len = length(output.p1);
	return output;
}