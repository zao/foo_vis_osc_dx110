@set "PATH=C:\Program Files (x86)\Windows Kits\8.1\bin\x86;%PATH%"
@echo 5_0 4_1 [4_0] [4_0_level_9_3] 4_0_level_9_3 4_0_level_9_0

:foo
@echo Fast Prefiltered Lines
fxc.exe /O3 /T vs_4_0           /Vn g_fpl_vs_4_0          /Fo g_fpl_vs_4_0.cso          /Fh g_fpl_vs_4_0.h     .\g_fpl_vs.hlsl && ^
fxc.exe /O3 /T ps_4_0           /Vn g_fpl_ps_4_0          /Fo g_fpl_ps_4_0.cso          /Fh g_fpl_ps_4_0.h     .\g_fpl_ps.hlsl && ^
fxc.exe /O3 /T vs_4_0_level_9_3 /Vn g_fpl_vs_4_0_9_3      /Fo g_fpl_vs_4_0_9_3.cso      /Fh g_fpl_vs_4_0_9_3.h .\g_fpl_vs.hlsl && ^
fxc.exe /O3 /T ps_4_0_level_9_3 /Vn g_fpl_ps_4_0_9_3      /Fo g_fpl_ps_4_0_9_3.cso      /Fh g_fpl_ps_4_0_9_3.h .\g_fpl_ps.hlsl
@echo Compose
fxc.exe /O3 /T vs_4_0_level_9_3 /Vn g_compose_line_vs /Fo g_compose_line_vs.cso /Fh g_compose_line_vs.h .\g_compose_line_vs.hlsl && ^
fxc.exe /O3 /T ps_4_0_level_9_3 /Vn g_compose_line_ps /Fo g_compose_line_ps.cso /Fh g_compose_line_ps.h .\g_compose_line_ps.hlsl
@echo Done!
timeout 1
goto foo