#include "zvfs_client.h"
#include <unordered_map>

#if defined(_WIN32)
#include <Windows.h>
namespace util {
	struct Module {
		explicit Module(std::string filename) {
			_module = LoadLibraryA(filename.c_str());
		}

		~Module() {
			if (loaded()) {
				FreeLibrary(_module);
			}
		}

		Module(Module const&) = delete;
		Module& operator=(Module const&) = delete;

		bool loaded() const { return _module && _module != INVALID_HANDLE_VALUE; }
	
		template <typename Signature>
		Signature get_function(std::string name) {
			if (loaded()) return (Signature)GetProcAddress(_module, name.c_str());
			return nullptr;
		}

		HMODULE _module = nullptr;
	};
}
#else
#include <dlfcn.h>
namespace util {
	struct Module {
		explicit Module(std::string filename) {
			_module = dlopen(filename.c_str(), 0);
		}

		~Module() {
			if (loaded()) {
				dlclose(_module);
			}
		}

		Module(Module const&) = delete;
		Module& operator=(Module const&) = delete;

		bool loaded() const { return _module; }
	
		template <typename Signature>
		Signature get_function(std::string name) {
			if (loaded()) return (Signature)dlsym(_module, name.c_str());
			return nullptr;
		}

		void* _module = nullptr;
	};
}
#endif

namespace vfs {
	typedef void* (*GetFunnelInterface)(uint32_t);
	struct FunnelV1 {
		void (*release_data)(void* data);
		bool (*get_resource_contents)(char const* name, void** data_out, int64_t* size_out);
		bool (*get_resource_modified_time)(char const* name, int64_t* time_out);
	};
	
	struct Entry {
		Blob data;
		int64_t timestamp = 0;
	};

	struct State {
		State() : _module(std::make_unique<util::Module>("funnel.dll")) {
			if (_module->loaded()) {
				GetFunnelInterface gfi = _module->get_function<GetFunnelInterface>("get_funnel_interface");
				if (gfi) {
					_api = (FunnelV1*)gfi(1);
				}
			}
		}
		~State() {}

		Entry const* get_fallback_resource(ResourceName name) const {
			auto I = _fallback_resources.find(name);
			if (I != _fallback_resources.end()) {
				return &I->second;
			}
			return nullptr;
		}

		std::unique_ptr<util::Module> _module = nullptr;
		FunnelV1* _api = nullptr;
		std::unordered_map<std::string, Entry> _fallback_resources;
	};

	static std::shared_ptr<State> g_state;
	Scope scope() {
		if (!g_state) {
			g_state = std::make_shared<State>();
		}
		return {g_state};
	}

	Timestamp get_resource_modification_time(ResourceName resource){
		int64_t t = 0;
		if (g_state->_api) {
			if (g_state->_api->get_resource_modified_time(resource.c_str(), &t)) {
				return t;
			}
		}
		auto entry = g_state->get_fallback_resource(resource);
		if (entry)
			t = entry->timestamp;
		return t;
	}

	std::unique_ptr<Blob> get_resource_contents(ResourceName resource) {
		std::unique_ptr<Blob> ret;
		if (g_state->_api) {
			void* p = nullptr;
			int64_t n = 0;
			if (g_state->_api->get_resource_contents(resource.c_str(), &p, &n)) {
				ret = std::make_unique<Blob>((char*)p, (char*)p + n);
				g_state->_api->release_data(p);
				return ret;
			}
		}
		auto entry = g_state->get_fallback_resource(resource);
		if (entry)
			ret = std::make_unique<Blob>(entry->data);
		return ret;
	}

	void store_fallback_resource(ResourceName name, Blob data, Timestamp timestamp) {
		Entry e;
		e.data = data;
		e.timestamp = timestamp;
		g_state->_fallback_resources[name] = e;
	}
}