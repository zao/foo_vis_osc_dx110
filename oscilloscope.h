#pragma once
#include "linalg.h"
#include "view.h"
#include <array>
#include <functional>
#include <random>
#include <tuple>
#include "zvfs_client.h"

struct RefreshableResource {
	void set_if_empty(std::string resource_name);

	void refresh();

	vfs::Blob const& contents();
	bool is_new() const;

private:
	vfs::Scope _scope;
	std::string _resource_name;
	std::unique_ptr<vfs::Blob> _contents;
	vfs::Timestamp _timestamp = 0;
	bool _new = false;
};

void edge_equations_for_fat_line(float width, Float2 p0, Float2 p1, Float3* edges);

template <typename T>
static void create_or_update_buffer(CComPtr<ID3D11Device> device,
									CComPtr<ID3D11DeviceContext> ctx,
									std::vector<T> const& data,
									CComPtr<ID3D11Buffer>& buffer,
									int& num_buffer_elements,
									D3D11_BIND_FLAG bind_flags =
										D3D11_BIND_VERTEX_BUFFER) {
	HRESULT hr = S_OK;
	auto n = data.size();
	int num_old_bytes = sizeof(T) * num_buffer_elements;
	int num_new_bytes = sizeof(T) * n;
	if (buffer && num_old_bytes == num_new_bytes) {
		D3D11_MAPPED_SUBRESOURCE map = {};
		hr = ctx->Map(buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &map);
		memcpy(map.pData, data.data(), num_new_bytes);
		ctx->Unmap(buffer, 0);
	}
	else {
		buffer.Release();
		num_buffer_elements = n;
		if (n == 0) return;
		auto byte_width = (UINT)(num_new_bytes);
		auto srd = [&] {
			D3D11_SUBRESOURCE_DATA _ = {};
			_.pSysMem = data.data();
			return _;
		}();
		hr = device->CreateBuffer(
			&CD3D11_BUFFER_DESC(
					byte_width, bind_flags, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE),
			&srd, &buffer);
	}
}

struct Timer {
	Timer();

	void reset();

	void update();
	int64_t elapsed();
	int64_t frequency() const;

	LARGE_INTEGER _base;
	LARGE_INTEGER _last;
	LARGE_INTEGER _frequency;
};

template <typename T, size_t N>
struct BufferChain {
	static_assert(N >= 2, "A buffer chain needs at least two buffers.");
	std::vector<T>& front() { return _buffers[_front]; }
	std::vector<T>& back() { return _buffers[_back]; }
	void flip() {
		_front = (_front+1)%2;
		_back = (_back+1)%2;
	}

public:
	int _front = 0;
	int _back = 1;
	std::array<std::vector<T>, N> _buffers;
};

struct Stuff {
	D3D11_PRIMITIVE_TOPOLOGY primitive_topology;
	CComPtr<ID3D11InputLayout> input_layout;
	CComPtr<ID3D11VertexShader> vertex_shader;
	CComPtr<ID3D11PixelShader> pixel_shader;
};

class SplineDrawer {
	struct Vertex {
		float x, y;
	};

	struct FPLInstance {
		Float2 from, to;
		float width;
		float r;
		Float3 edges[4];
	};
public:
	explicit SplineDrawer(View* view);

	void load_fixed_assets();
	void load_refreshable_assets();

	struct Data {
		int num_channels;
		float zoom;
		float left_time, right_time;
		float const* samples;
		size_t num_frames;
	};

	void update_spline(Data data);
	void update();

	void apply_stuff(CComPtr<ID3D11DeviceContext>& ctx, Stuff& stuff);

	void draw();

private:
	struct Particle {
		float x, y;
		float vx, vy;
		float age; float lifespan;
	};

	vfs::Scope _vfs_scope = vfs::scope();
	Timer _timer;
	View* _view;
	
	int64_t _last_update;
	int64_t _last_resource_refresh;

	RefreshableResource _fpl_vs_resource, _fpl_ps_resource;
	CComPtr<ID3D11Buffer> _window_extents_cb;
	CComPtr<ID3D11Buffer> _fpl_vertex_ids;
	CComPtr<ID3D11Buffer> _fpl_indices;
	CComPtr<ID3D11Buffer> _fpl_instances;
	CComPtr<ID3D11BlendState> _max_blend_state;
	int _num_fpl_instances = 0;
	Stuff _fpl_stuff;
	vfs::Timestamp _fpl_vs_timestamp = -1, _fpl_ps_timestamp = -1;

	RefreshableResource _line_compose_vs_resource, _line_compose_ps_resource;
	CComPtr<ID3D11SamplerState> _line_compose_sampler;
	CComPtr<ID3D11BlendState> _alpha_blend_state;
	Stuff _line_compose_stuff;
#if SWEET_PARTICLES
	BufferChain<Particle, 2> _particles;
	Stuff _particle_stuff;
	CComPtr<ID3D11Buffer> _particle_buffer;
	int _num_particles = 0;
#endif
};