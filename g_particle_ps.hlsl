struct PSInput {
	float4 pos : SV_Position;
};

float4 main(PSInput input) : SV_Target {
	return float4(0.5, 0.5, 0.5, 1.0);
}