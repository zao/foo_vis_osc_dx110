use strict;
use 5.14.0;
use Data::Dumper;
use Set::Light;
use Set::Scalar;
use List::MoreUtils qw/ uniq /;

my $fxc_dir = 'C:\Program Files (x86)\Windows Kits\8.1\bin\x86';

sub compile {
	my $args = shift;
	say "Compling $args->{'source_file'}.";

	my @args = "${fxc_dir}/fxc.exe";
	push @args, "/O$args->{'optimize'}" if ($args->{'optimize'});
	push @args, "/T", $args->{'model'} if ($args->{'model'});
	if ($args->{'header_file'}) {
		push @args, "/Fh", $args->{'header_file'};
		push @args, "/Vn", $args->{'varname'} if ($args->{'varname'});
	}
	push @args, "/Fo", $args->{'object_file'} if ($args->{'object_file'});
	push @args, $args->{'source_file'};
	system(@args);
	say $_;
}

my $files = {};
my $rules = {};

sub register {
	my $recipe = shift;

	my $input = $recipe->{source_file};
	$files->{$input} = {} unless $files->{$input};

	foreach my $output_key (qw/header_file object_file/) {
		if (my $target = $recipe->{$output_key}) {
			$files->{$target} = {} unless $files->{$target};
			$rules->{$input}->{$target} = $recipe;
		}
	}
}

sub gather_roots {
	my @ret;
	for my $file (keys $files) {
		say 
		push @ret, $file if (0 == $files->{$file});
	}
	return \@ret;
}

sub filter_subtrees {
	my $roots = shift;
	
	my $nodes = {};
	my $edges = {};

	my @q = @$roots;
	while (@q) {
		my $cur = shift @q;
		continue if (defined($nodes->{$cur}));
		if (my $children = $rules->{$cur}) {
			$nodes->{$cur} = 0;
			my $edge = Set::Light->new;
			for my $target (keys $children) {
				$edge->insert($target);
				push @q, $target;
			}
			$edges->{$cur} = $edge;
		}
	}
	for my $v0 (keys $edges) {
		for my $v1 (keys %{$edges->{$v0}}) {
			$nodes->{$v1}++;
		}
	}
	return ($nodes, $edges);
}

# L ← Empty list that will contain the sorted elements
# S ← Set of all nodes with no incoming edges
# while S is non-empty do
#     remove a node n from S
#     insert n into L
#     for each node m with an edge e from n to m do
#         remove edge e from the graph
#         if m has no other incoming edges then
#             insert m into S
# if graph has edges then
#     return error (graph has at least one cycle)
# else 
#     return L (a topologically sorted order)

sub collect_children {
	my $roots = shift;

	my ($nodes, $edges) = filter_subtrees($roots);
	#say "Subgraph:\n",
	#    "\tNodes: ", Dumper($nodes),
	#    "\tEdges: ", Dumper($edges);
	
	my @L = ();
	my $S = Set::Scalar->new;
	for my $k (keys $nodes) {
		$S->insert($k) if $nodes->{$k} == 0;
	}
	while (!$S->is_empty) {
		my $n = $S->each;
		$S->delete($n);
		push @L, $n;
		my $outgoing = delete $edges->{$n};
		for my $target (keys %$outgoing) {
			$S->insert($target) if (0 == --$nodes->{$target});
		}
	}
	return \@L;
}

register {
	"transform" => \&compile,
	"optimize" => 3,
	"model" => "vs_4_0",
	"varname" => "g_fpl_vs_4_0",
	"header_file" => "g_fpl_vs_4_0.h",
	"object_file" => "g_fpl_vs_4_0.cso",
	"source_file" => "g_fpl_vs.hlsl",
};

register {
	"transform" => \&compile,
	"optimize" => 3,
	"model" => "vs_4_0_level_9_3",
	"varname" => "g_fpl_vs_4_0_9_3",
	"header_file" => "g_fpl_vs_4_0_9_3.h",
	"object_file" => "g_fpl_vs_4_0_9_3.cso",
	"source_file" => "g_fpl_vs.hlsl",
};

register {
	"transform" => \&compile,
	"optimize" => 3,
	"model" => "ps_4_0",
	"varname" => "g_fpl_ps_4_0",
	"header_file" => "g_fpl_ps_4_0.h",
	"object_file" => "g_fpl_ps_4_0.cso",
	"source_file" => "g_fpl_ps.hlsl",
};

register {
	"transform" => \&compile,
	"optimize" => 3,
	"model" => "ps_4_0_level_9_3",
	"varname" => "g_fpl_ps_4_0_9_3",
	"header_file" => "g_fpl_ps_4_0_9_3.h",
	"object_file" => "g_fpl_ps_4_0_9_3.cso",
	"source_file" => "g_fpl_ps.hlsl",
};

register {
	"transform" => \&compile,
	"optimize" => 3,
	"model" => "vs_4_0_level_9_3",
	"varname" => "g_compose_line_vs",
	"object_file" => "g_compose_line_vs.cso",
	"header_file" => "g_compose_line_vs.h",
	"source_file" => "g_compose_line_vs.hlsl",
};

register {
	"transform" => \&compile,
	"optimize" => 3,
	"model" => "ps_4_0_level_9_3",
	"varname" => "g_compose_line_ps",
	"object_file" => "g_compose_line_ps.cso",
	"header_file" => "g_compose_line_ps.h",
	"source_file" => "g_compose_line_ps.hlsl",
};

#say Dumper($files);
#say Dumper($rules);

my $foo = 3;
my $bar = 4;
my $baz = 4;

my $victims = collect_children [keys $files];
for my $victim (@$victims) {
	my $artifacts = $rules->{$victim};
	if ($artifacts) {
		my @recipes = uniq (values $artifacts);
		for my $recipe (@recipes) {
			my $xform = $recipe->{'transform'};
			&$xform($recipe);
		}
	}
}

while (1) {
	sleep 1;
}

# :foo
# @echo Fast Prefiltered Lines
# fxc.exe /O3 /T vs_4_0           /Vn g_fpl_vs_4_0          /Fo g_fpl_vs_4_0.cso          /Fh g_fpl_vs_4_0.h     .\g_fpl_vs.hlsl && ^
# fxc.exe /O3 /T ps_4_0           /Vn g_fpl_ps_4_0          /Fo g_fpl_ps_4_0.cso          /Fh g_fpl_ps_4_0.h     .\g_fpl_ps.hlsl && ^
# fxc.exe /O3 /T vs_4_0_level_9_3 /Vn g_fpl_vs_4_0_9_3      /Fo g_fpl_vs_4_0_9_3.cso      /Fh g_fpl_vs_4_0_9_3.h .\g_fpl_vs.hlsl && ^
# fxc.exe /O3 /T ps_4_0_level_9_3 /Vn g_fpl_ps_4_0_9_3      /Fo g_fpl_ps_4_0_9_3.cso      /Fh g_fpl_ps_4_0_9_3.h .\g_fpl_ps.hlsl
# @echo Compose
# fxc.exe /O3 /T vs_4_0_level_9_3 /Vn g_compose_line_vs /Fo g_compose_line_vs.cso /Fh g_compose_line_vs.h .\g_compose_line_vs.hlsl && ^
# fxc.exe /O3 /T ps_4_0_level_9_3 /Vn g_compose_line_ps /Fo g_compose_line_ps.cso /Fh g_compose_line_ps.h .\g_compose_line_ps.hlsl
# @echo Done!
# timeout 1
# goto foo