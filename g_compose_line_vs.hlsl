struct VSInput {
	int2 vertex_id : VERTEX_ID;
};

struct PSInput {
	float4 pos : SV_Position;
	float2 tc  : TEXCOORD0;
};

PSInput main(VSInput input) {
	PSInput output;
	uint vtx_id = (uint)abs(input.vertex_id.x);
	float x_id = (vtx_id%2);
    float y_id = (vtx_id/2);

    float2 v = float2(
    	+ (x_id*4) - 1.0,
        1.0 - (y_id*4));
    float2 tc = float2(x_id*2, y_id*2);
//    v = mid 
//        + (x_id*2 - 1.0) * float2(window_extents.x, 0)
//        - (y_id*2 - 1.0) * float2(0, window_extents.y);
	output.pos = float4(v, 0.5, 1.0);
    output.tc = tc;
	return output;
}