struct PSInput {
	float4 pos      : SV_Position;
	float2 p        : REL_P;
	float2 p1       : REL_P1;
	float2 major    : MAJOR_AXIS;
	float inv_width : INV_WIDTH;
	float p1_len    : LEN_P1;
};

float4 main(PSInput input) : SV_Target {
	float inv_width = input.inv_width;
	float2 p1 = input.p1;
	float2 p = input.p;

	float d;
	float len = input.p1_len;
	float2 along = input.major;
	float along_t = dot(along/len, p);

	if (0 < along_t && along_t < 1) {
		float2 across = float2(along.y, -along.x);
		float d2 = abs(dot(float3(across, 0), float3(p, 1)) * inv_width);
		d = 1.0 - saturate(d2);
	}
	else {
		float d0 = 1.0 - saturate(length(p)      * inv_width);
		float d1 = 1.0 - saturate(length(p - p1) * inv_width);
		d = max(d0, d1);
	}

	float ss = smoothstep(0, 1, d);
	float I = ss*ss;
	float4 color = float4(I.xxxx);
	return color;
}