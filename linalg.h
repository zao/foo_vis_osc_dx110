#pragma once
#include <cmath>

struct Float2 {
	float x, y;
};

struct Float3 {
	float x, y, z;
};

inline Float2 operator+(Float2 a, Float2 b) {
	return {a.x + b.x, a.y + b.y};
}

inline Float2 operator+(Float2 a, float f) {
	return {a.x + f, a.y + f};
}

inline Float2 operator+(float f, Float2 b) {
	return {f + b.x, f + b.y};
}

inline Float2 operator-(Float2 a, Float2 b) {
	return {a.x - b.x, a.y - b.y};
}

inline Float2 operator-(Float2 a, float f) {
	return {a.x - f, a.y - f};
}

inline Float2 operator-(float f, Float2 b) {
	return {f - b.x, f - b.y};
}

inline Float2 operator*(Float2 a, Float2 b) {
	return {a.x * b.x, a.y * b.y};
}

inline Float2 operator*(Float2 a, float f) {
	return {a.x * f, a.y * f};
}

inline Float2 operator*(float f, Float2 b) {
	return {f * b.x, f * b.y};
}

inline Float2 operator/(Float2 a, float f) {
	f = 1.0f/f;
	return a * f;
}

inline Float2 operator-(Float2 v) {
	return {-v.x, -v.y};
}


inline Float3 operator+(Float3 a, Float3 b) {
	return {a.x + b.x, a.y + b.y, a.z + b.z};
}

inline Float3 operator+(Float3 a, float f) {
	return {a.x + f, a.y + f, a.z + f};
}

inline Float3 operator+(float f, Float3 b) {
	return {f + b.x, f + b.y, f + b.z};
}

inline Float3 operator-(Float3 a, Float3 b) {
	return {a.x - b.x, a.y - b.y, a.z - b.z};
}

inline Float3 operator-(Float3 a, float f) {
	return {a.x - f, a.y - f, a.z - f};
}

inline Float3 operator-(float f, Float3 b) {
	return {f - b.x, f - b.y, f - b.z};
}

inline Float3 operator*(Float3 a, Float3 b) {
	return {a.x * b.x, a.y * b.y, a.z * b.z};
}

inline Float3 operator*(Float3 a, float f) {
	return {a.x * f, a.y * f, a.z * f};
}

inline Float3 operator*(float f, Float3 b) {
	return {f * b.x, f * b.y, f * b.z};
}

inline Float3 operator/(Float3 a, float f) {
	f = 1.0f/f;
	return a * f;
}

inline Float3 operator-(Float3 v) {
	return {-v.x, -v.y, -v.z};
}

inline float dot(Float2 a, Float2 b) { return a.x * b.x + a.y * b.y; }
inline float dot(Float3 a, Float3 b) { return a.x * b.x + a.y * b.y + a.z * b.z; }

inline float length(Float2 v) { return sqrt(v.x * v.x + v.y * v.y); }
inline float length(Float3 v) { return sqrt(v.x * v.x + v.y * v.y + v.z * v.z); }

inline float length2(Float2 v) { return v.x * v.x + v.y * v.y; }
inline float length2(Float3 v) { return v.x * v.x + v.y * v.y + v.z * v.z; }

inline Float2 perpendicular(Float2 v) {
	return {v.y, -v.x};
}