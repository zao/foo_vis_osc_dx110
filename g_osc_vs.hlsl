struct VSInput {
	float2 pos : POSITION;
};

struct PSInput {
	float4 pos : SV_Position;
};

PSInput main(VSInput input) {
	PSInput output;
	output.pos = float4(input.pos, 0.5f, 1.0f);
	return output;
}