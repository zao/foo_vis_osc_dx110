#include <cstdint>
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace vfs {
	typedef std::vector<char> Blob;
	typedef std::string ResourceName;
	typedef int64_t Timestamp;

	Timestamp get_resource_modification_time(ResourceName resource);
	std::unique_ptr<Blob> get_resource_contents(ResourceName resource);
	void store_fallback_resource(ResourceName name, Blob data, Timestamp timestamp);

	struct Scope {
		std::shared_ptr<void> _;
	};
	Scope scope();
}