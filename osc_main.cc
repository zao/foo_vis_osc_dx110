#undef BOOST_ALL_NO_LIB
#define BOOST_ASIO_NO_WIN32_LEAN_AND_MEAN 1
#define BOOST_AUTO_LINK_TAGGED 1
#define BOOST_ASIO_DISABLE_IOCP 1
#include <boost/chrono.hpp>
#include <boost/system/windows_error.hpp>
#include <boost/asio.hpp>
#include <boost/asio/high_resolution_timer.hpp>
#include "../SDK/foobar2000.h"
#include "../ATLHelpers/ATLHelpers.h"
#include "oscilloscope.h"
#include "view.h"
#include <atomic>
#include <functional>
#include <future>
#include <memory>
#include <mutex>
#include <thread>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include "resource.h"
#include <json/json.h>

struct OscilloscopeConfig {
	enum class Mixing {
		IDENTITY = 0, DOWNMIX = 1
	};
	enum class Quality {
		NORMAL = 0, LOW = 1
	};
	enum class Duration {
		MS_100 = 100,
		MS_200 = 200,
		MS_300 = 300,
		MS_400 = 400,
		MS_500 = 500,
		MS_600 = 600,
		MS_700 = 700,
		MS_800 = 800
	};
	enum class Zoom {
		PERCENT_50  = 50,
		PERCENT_75  = 75,
		PERCENT_100 = 100,
		PERCENT_150 = 150,
		PERCENT_200 = 200,
		PERCENT_300 = 300,
		PERCENT_400 = 400,
		PERCENT_600 = 600,
		PERCENT_800 = 800,
	};

	Mixing mixing = Mixing::IDENTITY;
	Quality quality = Quality::NORMAL;
	Duration duration = Duration::MS_300;
	Zoom zoom = Zoom::PERCENT_100;
};

template <typename E>
typename std::enable_if<std::is_enum<E>::value, Json::Value>::type to_json(E e) {
	return (int)e;
}

template <typename E>
typename std::enable_if<std::is_enum<E>::value, E>::type from_json(Json::Value v) {
	return (E)v.asInt();
}

Json::Value to_json(OscilloscopeConfig cfg) {
	Json::Value v;
	v["mixing"] = to_json(cfg.mixing);
	v["quality"] = to_json(cfg.quality);
	v["duration"] = to_json(cfg.duration);
	v["zoom"] = to_json(cfg.zoom);
	return v;
}

OscilloscopeConfig from_json(Json::Value v) {
	OscilloscopeConfig cfg;
	auto members = v.getMemberNames();
	std::unordered_set<std::string> keys(std::begin(members), std::end(members));
	if (keys.count("mixing")) cfg.mixing = from_json<decltype(cfg.mixing)>(v["mixing"]);
	if (keys.count("quality")) cfg.quality = from_json<decltype(cfg.quality)>(v["quality"]);
	if (keys.count("duration")) cfg.duration = from_json<decltype(cfg.duration)>(v["duration"]);
	if (keys.count("zoom")) cfg.zoom = from_json<decltype(cfg.zoom)>(v["zoom"]);
	return cfg;
}

OscilloscopeConfig::Mixing toggle(OscilloscopeConfig::Mixing t) {
	typedef OscilloscopeConfig::Mixing T;
	T ts[] = { T::DOWNMIX, T::IDENTITY };
	return ts[(int)t];
}

OscilloscopeConfig::Quality toggle(OscilloscopeConfig::Quality t) {
	typedef OscilloscopeConfig::Quality T;
	T ts[] = { T::LOW, T::NORMAL };
	return ts[(int)t];
}

bool should_check(OscilloscopeConfig::Mixing t) {
	bool bs[] = { false, true };
	return bs[(int)t];
}

bool should_check(OscilloscopeConfig::Quality t) {
	bool bs[] = { false, true };
	return bs[(int)t];
}

template <typename E>
struct EnumMap {
	E to_enum(int id) { return _to_enum[id]; }
	int to_id(E e) { return _to_id[e]; }

	std::unordered_map<E, int> _to_id;
	std::unordered_map<int, E> _to_enum;
};

auto duration_mapping = [] {
	typedef OscilloscopeConfig::Duration E;
	EnumMap<E> _;
#define MAP_DURATION(N)                               \
	_._to_enum[ID_CURVEDURATION_##N##MS] = E::MS_##N; \
	_._to_id[E::MS_##N] = ID_CURVEDURATION_##N##MS;
	MAP_DURATION(100)
	MAP_DURATION(200)
	MAP_DURATION(300)
	MAP_DURATION(400)
	MAP_DURATION(500)
	MAP_DURATION(600)
	MAP_DURATION(700)
	MAP_DURATION(800)
#undef MAP_DURATION
	return _;
}();

auto zoom_mapping = [] {
	typedef OscilloscopeConfig::Zoom E;
	EnumMap<E> _;
#define MAP_ZOOM(N)                      \
	_._to_enum[ID_ZOOM_##N] = E::PERCENT_##N; \
	_._to_id[E::PERCENT_##N] = ID_ZOOM_##N;
	MAP_ZOOM(50)
	MAP_ZOOM(75)
	MAP_ZOOM(100)
	MAP_ZOOM(150)
	MAP_ZOOM(200)
	MAP_ZOOM(300)
	MAP_ZOOM(400)
	MAP_ZOOM(600)
	MAP_ZOOM(800)
#undef MAP_ZOOM
	return _;
}();

class VisWindow : public CWindowImpl<VisWindow> {
public:
	DECLARE_WND_CLASS_EX(L"osc_dx110_dui", CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, 0)

	enum { TICK_MESSAGE = WM_USER + 1 };

	BEGIN_MSG_MAP(VisWindow)
		MSG_WM_CREATE(on_wm_create)
		MSG_WM_DESTROY(on_wm_destroy)
		MSG_WM_SIZE(on_wm_size)
		MSG_WM_PAINT(on_wm_paint)
		MSG_WM_LBUTTONDBLCLK(on_wm_lbuttondblclk)
		MSG_WM_CONTEXTMENU(on_wm_contextmenu)
		MSG_WM_ERASEBKGND(on_wm_erasebkgnd)
		MESSAGE_HANDLER(TICK_MESSAGE, on_time_tick)
	END_MSG_MAP()

private:
	struct MenuItemContext {
		CMenuHandle h;
		int offset;
	};

	static std::unordered_map<int, MenuItemContext> gather_menu_contexts(CMenuHandle h) {
		std::unordered_map<int, MenuItemContext> menu_elements;
		std::function<void (CMenuHandle)> gather_elements = [&](CMenuHandle h) {
			auto& _ = menu_elements;
			auto n = h.GetMenuItemCount();
			for (int i = 0; i < n; ++i) {
				auto sub = h.GetSubMenu(i);
				if (sub.IsMenu())
					gather_elements(h.GetSubMenu(i));
				else {
					auto id = h.GetMenuItemID(i);
					if (id) _[id] = {h, i};
				}
			}
		};
		gather_elements(h);
		return menu_elements;
	}

	template <typename Enum>
	static UINT make_checking_flags(Enum e) {
		return MF_BYPOSITION | (should_check(e) ? MF_CHECKED : MF_UNCHECKED);
	};

	LRESULT on_wm_create(CREATESTRUCT* cs) {
		timeBeginPeriod(1);
		_view = std::make_unique<View>();
		_view->set_window(*this);
		_spline_drawer = std::make_unique<SplineDrawer>(_view.get());
		static_api_ptr_t<visualisation_manager>()->create_stream(_vis_stream, visualisation_manager::KStreamFlagNewFFT);
		_vis_stream->set_channel_mode(
			(_config.mixing == OscilloscopeConfig::Mixing::DOWNMIX)
			? visualisation_stream_v2::channel_mode_mono
			: visualisation_stream_v2::channel_mode_default);
		_context_menu = make_context_menu();
		_context_contexts = gather_menu_contexts((HMENU)*_context_menu);
		auto check_menu_radio_item = [](MenuItemContext ctx) {
			ctx.h.CheckMenuRadioItem(0,
									 ctx.h.GetMenuItemCount() - 1,
									 ctx.offset,
									 MF_BYPOSITION | MF_CHECKED);
		};
		{
			auto& _ = _context_actions;
			_[ID_TOGGLEFULLSCREENMODE] = [&] {
				auto& ctx = this->_context_contexts[ID_TOGGLEFULLSCREENMODE];
				// make_fullscreen_window();
				// or
				// destroy_fullscreen_window();
			};
			_[ID_DOWNMIXCHANNELS] = [&] {
				auto& ctx = this->_context_contexts[ID_DOWNMIXCHANNELS];
				_config.mixing = toggle(_config.mixing);
				ctx.h.CheckMenuItem(ctx.offset,
									make_checking_flags(_config.mixing));
				_vis_stream->set_channel_mode(
					(_config.mixing == OscilloscopeConfig::Mixing::DOWNMIX)
					? visualisation_stream_v2::channel_mode_mono
					: visualisation_stream_v2::channel_mode_default);
			};
			_[ID_LOWQUALITYMODE] = [&] {
				auto& ctx = this->_context_contexts[ID_LOWQUALITYMODE];
				_config.quality = toggle(_config.quality);
				ctx.h.CheckMenuItem(ctx.offset,
									make_checking_flags(_config.quality));
			};
			int duration_ids[] = {
				ID_CURVEDURATION_100MS, ID_CURVEDURATION_200MS,
				ID_CURVEDURATION_300MS, ID_CURVEDURATION_400MS,
				ID_CURVEDURATION_500MS, ID_CURVEDURATION_600MS,
				ID_CURVEDURATION_700MS, ID_CURVEDURATION_800MS};
			for (int id : duration_ids) {
				_[id] = [&,id] {
					auto& ctx = this->_context_contexts[id];
					_config.duration = duration_mapping.to_enum(id);
					check_menu_radio_item(ctx);
				};
			}
			int zoom_ids[] = {ID_ZOOM_50,  ID_ZOOM_75,  ID_ZOOM_100,
							  ID_ZOOM_150, ID_ZOOM_200, ID_ZOOM_300,
							  ID_ZOOM_400, ID_ZOOM_600, ID_ZOOM_800};
			for (int id : zoom_ids) {
				_[id] = [&,id] {
					auto& ctx = this->_context_contexts[id];
					_config.zoom = zoom_mapping.to_enum(id);
					check_menu_radio_item(ctx);
				};
			}
		}

		{
			auto id = duration_mapping.to_id(_config.duration);
			_context_actions[id]();
		}
		{
			auto id = zoom_mapping.to_id(_config.zoom);
			_context_actions[id]();
		}
		{
			auto& ctx = this->_context_contexts[ID_DOWNMIXCHANNELS];
			ctx.h.CheckMenuItem(ctx.offset,
								make_checking_flags(_config.mixing));
		}
		{
			auto& ctx = this->_context_contexts[ID_LOWQUALITYMODE];
			ctx.h.CheckMenuItem(ctx.offset,
								make_checking_flags(_config.quality));
		}
		return 0;
	}

	void on_wm_destroy() {
		if (_repaint_timer_id) {
			_tick_timer->cancel();
			_tick_work.reset();
			_tick_thread->join();
		}
		_vis_stream.release();
		_spline_drawer.reset();
		_view.reset();
		timeEndPeriod(1);
	}

	void on_wm_size(UINT state, CSize size) {
		_view->reshape(size.cx, size.cy);
	}

	void on_wm_paint(HDC dc) {
		if (!_repaint_timer_id) {
			_tick_work = std::make_unique<boost::asio::io_service::work>(_io);
			_tick_timer = std::make_unique<boost::asio::high_resolution_timer>(_io);
			_tick_thread = std::make_unique<std::thread>([&]{_io.run();});
			_repaint_timer_id = 0x4201;
			PostMessage(TICK_MESSAGE);
		}
		ValidateRgn(nullptr);
	}

	LRESULT on_wm_erasebkgnd(CDCHandle dc) {
		return TRUE;
	}

	LRESULT on_time_tick(UINT message, WPARAM wparam, LPARAM lparam, BOOL& handled) {
		handled = TRUE;
		paint_one();
		auto duration = boost::chrono::milliseconds(5);
		_tick_timer->expires_from_now(duration);
		_tick_timer->async_wait(std::bind(&VisWindow::timer_timer_expired, this, std::placeholders::_1));
		return 0;
	}

	void timer_timer_expired(boost::system::error_code const& ec) {
		if (!ec) {
			PostMessage(TICK_MESSAGE);
		}
	}

	void paint_one() {
		float clear_color[] = {0.0f, 0.0f, 0.0f, 1.0f};
		_view->context->ClearRenderTargetView(_view->render_target_view.p,
												clear_color);
		if (_vis_stream.is_valid()) {
			static std::mt19937 rng;
			std::uniform_real<float> dist(-1.0f, 1.0f);
			double t = 0.0;
			_vis_stream->get_absolute_time(t);
			std::vector<float> spline;
			audio_chunk_impl_temporary chunk;
			auto const W = (int)_config.duration / 1000.0f;
			auto const zoom = (int)_config.zoom / 100.0f;
			_vis_stream->get_chunk_absolute(chunk, t - W / 2.0f, W);
			SplineDrawer::Data data = {};
			data.num_channels = chunk.get_channel_count();
			data.left_time = (float)(t - W / 2.0f);
			data.right_time = (float)(t + W / 2.0f);
			data.zoom = zoom;
			data.samples = chunk.get_data();
			data.num_frames = chunk.get_sample_count();
			_spline_drawer->update_spline(data);
			_spline_drawer->update();
			_spline_drawer->draw();
		}
		UINT present_interval = 1;
		DWORD present_flag = 0;
		_view->target()._swap_chain->Present(present_interval, present_flag);
	}

	void on_wm_lbuttondblclk(UINT flags, CPoint where) {
		console::printf("%d,%d doubleclick\n", where.x, where.y);
	}

	std::unique_ptr<CMenu> make_context_menu() {
		BOOL b = TRUE;
		CMenu h;
		b = h.LoadMenuW(IDR_CONFIG_CONTEXT);
		auto m = std::make_unique<CMenu>(h.GetSubMenu(0));
		b = h.RemoveMenu(0, MF_BYPOSITION);
		return m;
	}

	void on_wm_contextmenu(CWindow wnd, CPoint where) {
		if (forward_rightclick()) {
			SetMsgHandled(FALSE);
			return;
		}
		BOOL ans = _context_menu->TrackPopupMenu(
			TPM_NONOTIFY | TPM_RETURNCMD,
			where.x,
			where.y,
			*this,
			0);
		auto I = _context_actions.find((int)ans);
		if (I != _context_actions.end()) {
			I->second();
		}
	}

	std::unique_ptr<View> _view;
	std::unique_ptr<SplineDrawer> _spline_drawer;
	service_ptr_t<visualisation_stream_v3> _vis_stream;
	boost::asio::io_service _io;
	std::unique_ptr<boost::asio::io_service::work> _tick_work;
	std::unique_ptr<boost::asio::high_resolution_timer> _tick_timer;
	std::unique_ptr<std::thread> _tick_thread;
	UINT_PTR _repaint_timer_id = 0;

	struct MenuTemplate {
		std::vector<MenuTemplate> items;
		std::function<void()> on_activate;
	};

	std::unique_ptr<CMenu> _context_menu;
	std::unordered_map<int, MenuItemContext> _context_contexts;
	std::unordered_map<int, std::function<void()>> _context_actions;

protected:
	virtual bool forward_rightclick() { return false; }
	OscilloscopeConfig _config;
};

class VisUI : public ui_element_instance, public VisWindow {
public:
	static GUID g_get_guid() { return s_guid; }
	static GUID g_get_subclass() { return ui_element_subclass_playback_visualisation; }
	static void g_get_name(pfc::string_base& out) { out = "Oscilloscope (D3D11.0)"; }
	static char const* g_get_description() { return "An oscilloscope with GPU accelerated display."; }

	static ui_element_config::ptr g_get_default_configuration() {
		std::string data = "{}";
		return ui_element_config::g_create(s_guid, data.data(), data.size());
	}

	void set_configuration(ui_element_config::ptr data) {
		Json::Value root;
		Json::Reader r;
		auto p = (char const*)data->get_data();
		r.parse(p, p + data->get_data_size(), root);
		if (root.isMember("windowed"))
			_config = from_json(root["windowed"]);
	}

	ui_element_config::ptr get_configuration() {
		auto& cfg = _config;
		Json::Value root;
		root["windowed"] = to_json(cfg);
		Json::StyledWriter w;
		auto s = w.write(root);
		return ui_element_config::g_create(s_guid, s.data(), s.size());
	}

	void initialize_window(HWND parent) {
		Create(parent,
			   0,
			   nullptr,
			   WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
			   WS_EX_STATICEDGE);
	}

	void notify(GUID const& what, t_size param1, void const* param2, t_size param2_size) {}
	VisUI(ui_element_config::ptr config, ui_element_instance_callback::ptr callback) : _callback(callback) {
		set_configuration(config);
	}
	~VisUI() = default;

protected:
	virtual bool forward_rightclick() override {
		return _callback->is_edit_mode_enabled();
	}
	virtual bool edit_mode_context_menu_test(const POINT& p_point,
											 bool p_fromkeyboard) override {
		return true;
	}
	ui_element_instance_callback::ptr _callback;
	static GUID const s_guid;
};

// {DB082E80-CD55-4B7D-974E-AD770E7394F8}
__declspec(selectany) GUID const VisUI::s_guid =
{ 0xdb082e80, 0xcd55, 0x4b7d, { 0x97, 0x4e, 0xad, 0x77, 0xe, 0x73, 0x94, 0xf8 } };

#if 0
typedef ImplementBumpableElem<VisUI> UIElement;
#else
typedef VisUI UIElement;
#endif
static service_factory_t<ui_element_impl<UIElement>> g_asdf;

class FreestandingVis : public VisWindow {
public:
	FreestandingVis() { console::formatter() << "in Freestanding ctor\n"; }
	~FreestandingVis() { console::formatter() << "in Freestanding dtor\n"; }
};

class VisMenu : public mainmenu_commands {
public:
	t_uint32 get_command_count() override { return 1; }
	GUID get_command(t_uint32 index) override { return s_osc_item_guid; }
	void get_name(t_uint32 index, pfc::string_base& out) override { out = "Oscilloscope (D3D 11.0)"; }
	bool get_description(t_uint32 index, pfc::string_base& out) override { return false; }
	GUID get_parent() override { return mainmenu_groups::view_visualisations; }
	void execute(t_uint32 index, service_ptr_t<service_base>) override {
		std::thread([] {
			auto p = std::make_unique<FreestandingVis>();
			if (p->Create(nullptr,
						  nullptr,
						  L"Oscilloscope (D3D11.0)",
						  WS_OVERLAPPEDWINDOW | WS_VISIBLE))
			{
				MSG msg = {};
				while (GetMessage(&msg, 0, 0, 0)) {
					TranslateMessage(&msg);
					DispatchMessage(&msg);
					if (!p)
						break;
				}
			}
		}).detach();
	}

private:
	static GUID const s_osc_item_guid;
};

// {003E5B4A-FC8F-4442-8C20-3B931FCFBA9F}
GUID const VisMenu::s_osc_item_guid =
{ 0x3e5b4a, 0xfc8f, 0x4442, { 0x8c, 0x20, 0x3b, 0x93, 0x1f, 0xcf, 0xba, 0x9f } };

static mainmenu_commands_factory_t<VisMenu> g_vis_menu;

DECLARE_COMPONENT_VERSION("Oscilloscope (D3D11.0)", "0.13", "zao")
VALIDATE_COMPONENT_FILENAME("foo_vis_osc_dx110.dll")