#pragma once

#include <atlbase.h>
#include <atlcom.h>
#include <atltypes.h>
#include <memory>
#include <vector>
#include <d3d11.h>
#include <dxgi.h>

template <typename Child, typename Parent>
inline HRESULT get_parent(CComPtr<Child> const& child, CComPtr<Parent>& parent) {
	return child->GetParent(__uuidof(Parent), (void**)&parent);
}

class View {
public:
	enum class ScreenMode { Windowed, Fullscreen };
	View() {
		HRESULT hr = S_OK;
#if _DEBUG
		UINT flags = D3D11_CREATE_DEVICE_DEBUG;
#else
		UINT flags = 0;
#endif
#define DOWNLEVEL_PARTY_SUBMISSION_POSITION_ASSUMED 0
#if DOWNLEVEL_PARTY_SUBMISSION_POSITION_ASSUMED
		std::vector<D3D_FEATURE_LEVEL> feature_level_candidates = {D3D_FEATURE_LEVEL_9_3};
#else
		std::vector<D3D_FEATURE_LEVEL> feature_level_candidates = {
			D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0, D3D_FEATURE_LEVEL_9_3};
#endif
		hr = D3D11CreateDevice(nullptr,
							   D3D_DRIVER_TYPE_HARDWARE,
							   nullptr,
							   flags,
							   feature_level_candidates.empty()
								   ? nullptr
								   : feature_level_candidates.data(),
							   feature_level_candidates.size(),
							   D3D11_SDK_VERSION,
							   &device,
							   &feature_level,
							   &context);

		CComPtr<IDXGIDevice1> dxgi_device;
		hr = device->QueryInterface(&dxgi_device);
		CComPtr<IDXGIAdapter> dxgi_adapter;
		hr = get_parent(dxgi_device, dxgi_adapter);
		hr = get_parent(dxgi_adapter, dxgi_factory);

		CD3D11_RASTERIZER_DESC rasterizer_desc(D3D11_FILL_SOLID,
											   D3D11_CULL_NONE,
											   TRUE,
											   0,
											   0.0f,
											   0.0f,
											   TRUE,
											   FALSE,
											   FALSE,
											   FALSE);
		hr = device->CreateRasterizerState(&rasterizer_desc, &rasterizer_state);
		context->RSSetState(rasterizer_state);
	}

	void set_window(HWND wnd) {
		_component_window._wnd = wnd;
		CRect r = {};
		GetWindowRect(wnd, r);
		reshape(r.Width(), r.Height());
	}

	void reshape(int w, int h) {
		bool size_changed =
			_component_window._w != w || _component_window._h != h;
		_component_window._w = w;
		_component_window._h = h;
		if (_component_window._w == 0 || _component_window._h == 0)
			return;
		if (!size_changed)
			return;
		HRESULT hr = S_OK;

		auto make_swap_chain_desc = [&](Target& target, ScreenMode screen_mode) {
			auto wnd = target._wnd;
			CRect client = {};
			GetClientRect(target._wnd, &client);
			auto is_windowed = (screen_mode == ScreenMode::Windowed);
			DXGI_SWAP_CHAIN_DESC _ = {};
			_.BufferCount = 2;
			_.BufferDesc = [&] {
				DXGI_MODE_DESC _ = {};
				_.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
				_.Height = client.Height();
				if (is_windowed) {
					_.RefreshRate = {0, 1};
				} else {
					MONITORINFOEX mi = {};
					mi.cbSize = sizeof(mi);
					GetMonitorInfo(MonitorFromWindow(wnd, MONITOR_DEFAULTTONEAREST), &mi);
					DEVMODEW devmode = {};
					EnumDisplaySettings(nullptr, ENUM_CURRENT_SETTINGS, &devmode);
					_.RefreshRate = { devmode.dmDisplayFrequency, 1 };
				}
				_.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
				_.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
				_.Width = client.Width();
				return _;
			}();
			_.BufferUsage =
				DXGI_USAGE_BACK_BUFFER | DXGI_USAGE_RENDER_TARGET_OUTPUT;
			_.Flags = 0;
			_.OutputWindow = wnd;
			_.SampleDesc.Count = 1;
			_.SampleDesc.Quality = 0;
			_.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			_.Windowed = is_windowed ? TRUE : FALSE;
			return _;
		};
		
		auto& swap_chain = _component_window._swap_chain;
		auto swap_chain_desc = make_swap_chain_desc(_component_window, ScreenMode::Windowed);
		_component_window._swap_chain.Release();
		hr = dxgi_factory->CreateSwapChain(device, &swap_chain_desc, &swap_chain);

		CComPtr<ID3D11Texture2D> back_buffer;
		hr = swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&back_buffer);
		render_target_view.Release();
		hr = device->CreateRenderTargetView(back_buffer, nullptr, &render_target_view);
		context->OMSetRenderTargets(1, &render_target_view.p, nullptr);

		line_composition = {};

		CD3D11_TEXTURE2D_DESC line_texture_desc(
			DXGI_FORMAT_R8G8B8A8_UNORM, w, h, 1, 1,
			D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE);
		hr = device->CreateTexture2D(
			&line_texture_desc, nullptr, &line_composition.backing_texture);
		hr = device->CreateShaderResourceView(
			line_composition.backing_texture,
			nullptr,
			&line_composition.shader_resource_view);
		hr = device->CreateRenderTargetView(
			line_composition.backing_texture,
			nullptr,
			&line_composition.render_target_view);

		auto& viewport = _component_window._viewport;
		viewport = CD3D11_VIEWPORT(back_buffer, render_target_view);
		context->RSSetViewports(1, &viewport);
	}

public:
	struct Target {
		Target() : _wnd(nullptr), _w(0), _h(0) {}
		HWND _wnd;
		int _w, _h;
		D3D11_VIEWPORT _viewport;
		CComPtr<IDXGISwapChain> _swap_chain;
	};

	CComPtr<IDXGIFactory1> dxgi_factory;
	CComPtr<ID3D11Device> device;
	D3D_FEATURE_LEVEL feature_level;
	CComPtr<ID3D11DeviceContext> context;

	CComPtr<ID3D11RenderTargetView> render_target_view;
	CComPtr<ID3D11RasterizerState> rasterizer_state;

	struct RenderTargetBundle {
		CComPtr<ID3D11Texture2D> backing_texture;
		CComPtr<ID3D11ShaderResourceView> shader_resource_view;
		CComPtr<ID3D11RenderTargetView> render_target_view;
	};

	RenderTargetBundle line_composition;
	Target const& target() const { return _component_window; }

private:
	Target _component_window;
};