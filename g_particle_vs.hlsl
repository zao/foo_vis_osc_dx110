struct VSInput {
	float2 pos     : POSITION;
	float2 vel     : VELOCITY;
	float age      : AGE;
	float lifespan : LIFESPAN;
};

struct PSInput {
	float4 pos : SV_Position;
};

PSInput main(VSInput input) {
	PSInput output;
	output.pos = float4(input.pos, 0.4f, 1.0f);
	return output;
}