#include "zvfs_client.h"

template <size_t N>
vfs::Blob make_blob(unsigned char const (&arr)[N]) {
	return{ (char const*)arr, (char const*)arr + N };
}

void register_fallback_resources()
{
	typedef unsigned char BYTE;
#include "g_osc_vs_4_0.h"
#include "g_osc_vs_4_0_9_3.h"
#include "g_osc_ps_4_0.h"
#include "g_osc_ps_4_0_9_3.h"

#include "g_particle_vs_4_0.h"
#include "g_particle_vs_4_0_9_3.h"
#include "g_particle_ps_4_0.h"
#include "g_particle_ps_4_0_9_3.h"

#include "g_fpl_vs_4_0.h"
#include "g_fpl_vs_4_0_9_3.h"
#include "g_fpl_ps_4_0.h"
#include "g_fpl_ps_4_0_9_3.h"

#include "g_compose_line_vs.h"
#include "g_compose_line_ps.h"
	vfs::store_fallback_resource("/g_osc_vs_4_0.cso", make_blob(g_osc_vs_4_0), 0);
	vfs::store_fallback_resource("/g_osc_vs_4_0_9_3.cso", make_blob(g_osc_vs_4_0_9_3), 0);
	vfs::store_fallback_resource("/g_osc_ps_4_0.cso", make_blob(g_osc_ps_4_0), 0);
	vfs::store_fallback_resource("/g_osc_ps_4_0_9_3.cso", make_blob(g_osc_ps_4_0_9_3), 0);
	vfs::store_fallback_resource("/g_particle_vs_4_0.cso", make_blob(g_particle_vs_4_0), 0);
	vfs::store_fallback_resource("/g_particle_vs_4_0_9_3.cso", make_blob(g_particle_vs_4_0_9_3), 0);
	vfs::store_fallback_resource("/g_particle_ps_4_0.cso", make_blob(g_particle_ps_4_0), 0);
	vfs::store_fallback_resource("/g_particle_ps_4_0_9_3.cso", make_blob(g_particle_ps_4_0_9_3), 0);
	vfs::store_fallback_resource("/g_fpl_vs_4_0.cso", make_blob(g_fpl_vs_4_0), 0);
	vfs::store_fallback_resource("/g_fpl_vs_4_0_9_3.cso", make_blob(g_fpl_vs_4_0_9_3), 0);
	vfs::store_fallback_resource("/g_fpl_ps_4_0.cso", make_blob(g_fpl_ps_4_0), 0);
	vfs::store_fallback_resource("/g_fpl_ps_4_0_9_3.cso", make_blob(g_fpl_ps_4_0_9_3), 0);
	vfs::store_fallback_resource("/g_compose_line_vs.cso", make_blob(g_compose_line_vs), 0);
	vfs::store_fallback_resource("/g_compose_line_ps.cso", make_blob(g_compose_line_ps), 0);
}