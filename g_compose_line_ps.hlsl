struct PSInput {
	float4 pos : SV_Position;
	float2 tc  : TEXCOORD0;
};

cbuffer colors
{
    float4 foreground;
};

SamplerState smp;
Texture2D tex;

float4 main(PSInput input) : SV_Target {
	float3 fg = float3(176, 175, 157)/255;
	float3 bg = float3(22, 32, 41)/255;
	float alpha = tex.Sample(smp, input.tc).x;
	return float4(fg.rgb*alpha + bg.rgb*(1-alpha), 1.0);
	return float4(foreground.rgb, alpha);
}