//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by resource.rc
//
#define IDR_CONFIG_CONTEXT              101
#define ID_CURVEDURATION_100MS          40001
#define ID_CURVEDURATION_200MS          40002
#define ID_CURVEDURATION_300MS          40003
#define ID_CURVEDURATION_400MS          40004
#define ID_CURVEDURATION_500MS          40005
#define ID_CURVEDURATION_600MS          40006
#define ID_CURVEDURATION_700MS          40007
#define ID_CURVEDURATION_800MS          40008
#define ID_ZOOM_50                      40009
#define ID_ZOOM_75                      40010
#define ID_ZOOM_100                     40011
#define ID_ZOOM_150                     40012
#define ID_ZOOM_200                     40013
#define ID_ZOOM_300                     40014
#define ID_ZOOM_400                     40015
#define ID_ZOOM_600                     40016
#define ID_ZOOM_800                     40017
#define ID_TOGGLEFULLSCREENMODE         40018
#define ID_DOWNMIXCHANNELS              40019
#define ID_LOWQUALITYMODE               40020

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40021
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
