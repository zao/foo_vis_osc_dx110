#include "oscilloscope.h"

void RefreshableResource::set_if_empty(std::string resource_name) {
	if (!_contents) {
		_resource_name = resource_name;
		_timestamp = vfs::get_resource_modification_time(_resource_name);
		_contents = vfs::get_resource_contents(_resource_name);
		_new = true;
	}
}

void RefreshableResource::refresh() {
	auto ts = vfs::get_resource_modification_time(_resource_name);
	if (ts > _timestamp) {
		_timestamp = ts;
		_contents = vfs::get_resource_contents(_resource_name);
		_new = true;
	}
}

vfs::Blob const& RefreshableResource::contents() { _new = false; return *_contents; }
bool RefreshableResource::is_new() const { return _new; }

Timer::Timer() {
	QueryPerformanceFrequency(&_frequency);
	reset();
}

void Timer::reset() {
	QueryPerformanceCounter(&_base);
	_last = _base;
}

void Timer::update() { QueryPerformanceCounter(&_last); }
int64_t Timer::elapsed() { return _last.QuadPart - _base.QuadPart; }
int64_t Timer::frequency() const { return _frequency.QuadPart; }

void register_fallback_resources();

SplineDrawer::SplineDrawer(View* view) : _view(view)
{
	_timer.update();
	_last_resource_refresh = _timer.elapsed();
	register_fallback_resources();
	load_fixed_assets();
	load_refreshable_assets();
}

void SplineDrawer::load_fixed_assets() {
	HRESULT hr = S_OK;
	D3D11_BLEND_DESC blend_desc = [&] {
		CD3D11_BLEND_DESC _;
		_.AlphaToCoverageEnable = FALSE;
		_.IndependentBlendEnable = FALSE;
		_.RenderTarget[0] = [&] {
			D3D11_RENDER_TARGET_BLEND_DESC _ = {};
			_.BlendEnable = TRUE;
			_.BlendOp = D3D11_BLEND_OP_MAX;
			_.SrcBlend = D3D11_BLEND_ONE;
			_.DestBlend = D3D11_BLEND_ONE;
			_.BlendOpAlpha = D3D11_BLEND_OP_MAX;
			_.SrcBlendAlpha = D3D11_BLEND_ONE;
			_.DestBlendAlpha = D3D11_BLEND_ONE;
			_.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
			return _;
		}();
		return _;
	}();
	hr = _view->device->CreateBlendState(&blend_desc, &_max_blend_state);

	D3D11_BLEND_DESC alpha_blend_desc = [&] {
		CD3D11_BLEND_DESC _;
		_.AlphaToCoverageEnable = FALSE;
		_.IndependentBlendEnable = FALSE;
		_.RenderTarget[0] = [&] {
			D3D11_RENDER_TARGET_BLEND_DESC _ = {};
			_.BlendEnable = TRUE;
			_.BlendOp = D3D11_BLEND_OP_ADD;
			_.SrcBlend = D3D11_BLEND_SRC_ALPHA;
			_.DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
			_.BlendOpAlpha = D3D11_BLEND_OP_ADD;
			_.SrcBlendAlpha = D3D11_BLEND_ONE;
			_.DestBlendAlpha = D3D11_BLEND_ZERO;
			_.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
			return _;
		}();
		return _;
	}();
	hr = _view->device->CreateBlendState(&alpha_blend_desc, &_alpha_blend_state);

	// CComPtr<ID3D11SamplerState> _line_compose_sampler;
	auto sampler_desc = [&] {
		auto _ = CD3D11_SAMPLER_DESC(CD3D11_DEFAULT());
		_.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		return _;
	}();
	hr = _view->device->CreateSamplerState(&sampler_desc, &_line_compose_sampler);
}

Stuff make_stuff(
	ID3D11Device* device,
	vfs::Blob const& vs_bytecode,
	vfs::Blob const& ps_bytecode,
	std::vector<D3D11_INPUT_ELEMENT_DESC> const& ieds,
	D3D11_PRIMITIVE_TOPOLOGY topology)
{
	HRESULT hr = S_OK;
	Stuff ret;
	hr = device->CreateVertexShader(
		vs_bytecode.data(), vs_bytecode.size(), nullptr, &ret.vertex_shader);
	hr = device->CreatePixelShader(
		ps_bytecode.data(), ps_bytecode.size(), nullptr, &ret.pixel_shader);

	if (ieds.size()) {
		hr = device->CreateInputLayout(ieds.data(),
									   ieds.size(),
									   vs_bytecode.data(),
									   vs_bytecode.size(),
									   &ret.input_layout);
	}
	ret.primitive_topology = topology;
	return ret;
}

void SplineDrawer::load_refreshable_assets() {
	HRESULT hr = S_OK;
	auto level = _view->feature_level;
	bool downlevel = level < D3D_FEATURE_LEVEL_10_0;
	{
		_line_compose_vs_resource.set_if_empty("/g_compose_line_vs.cso");
		_line_compose_ps_resource.set_if_empty("/g_compose_line_ps.cso");

		_line_compose_vs_resource.refresh();
		_line_compose_ps_resource.refresh();

		if (_line_compose_vs_resource.is_new() || _line_compose_ps_resource.is_new()) {
			auto vs_bytecode = _line_compose_vs_resource.contents();
			auto ps_bytecode = _line_compose_ps_resource.contents();

			std::vector<D3D11_INPUT_ELEMENT_DESC> ieds {
				{ "VERTEX_ID", 0, DXGI_FORMAT_R16G16_SINT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			};
			_line_compose_stuff =
				make_stuff(_view->device,
						   vs_bytecode,
						   ps_bytecode,
						   ieds,
						   D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		}
	}
	{
		if (downlevel) {
			_fpl_vs_resource.set_if_empty("/g_fpl_vs_4_0_9_3.cso");
			_fpl_ps_resource.set_if_empty("/g_fpl_ps_4_0_9_3.cso");
		} else {
			_fpl_vs_resource.set_if_empty("/g_fpl_vs_4_0.cso");
			_fpl_ps_resource.set_if_empty("/g_fpl_ps_4_0.cso");
		}

		_fpl_vs_resource.refresh();
		_fpl_ps_resource.refresh();
		if (_fpl_vs_resource.is_new() || _fpl_ps_resource.is_new()) {
			auto vs_bytecode = _fpl_vs_resource.contents();
			auto ps_bytecode = _fpl_ps_resource.contents();

			std::vector<D3D11_INPUT_ELEMENT_DESC> ieds = {
				{ "VERTEX_ID", 0, DXGI_FORMAT_R16G16_SINT,        0, 0,                            D3D11_INPUT_PER_VERTEX_DATA,   0 },
				{ "POINT",     0, DXGI_FORMAT_R32G32_FLOAT,       1, 0,                            D3D11_INPUT_PER_INSTANCE_DATA, 1 },
				{ "POINT",     1, DXGI_FORMAT_R32G32_FLOAT,       1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
				{ "WIDTH",     0, DXGI_FORMAT_R32_FLOAT,          1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
				{ "R",         0, DXGI_FORMAT_R32_FLOAT,          1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			};
			_fpl_stuff = make_stuff(_view->device, vs_bytecode, ps_bytecode, ieds, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
		}

		if (!_fpl_indices) {
			std::vector<uint16_t> indices = { 0, 1, 2, 3 };
			int num_existing_indices = 0;
			create_or_update_buffer(_view->device,
									_view->context,
									indices,
									_fpl_indices,
									num_existing_indices,
									D3D11_BIND_INDEX_BUFFER);
		}

		if (!_fpl_vertex_ids) {
			std::vector<int16_t> vertex_ids = { 0, 0, 1, 1, 2, 2, 3, 3 };
			int num_existing_elements = 0;
			create_or_update_buffer(_view->device,
									_view->context,
									vertex_ids,
									_fpl_vertex_ids,
									num_existing_elements);
		}
	}
}

void SplineDrawer::update_spline(Data data) {
	HRESULT hr = S_OK;
	int n = data.num_frames;
	std::vector<Vertex> vs;
	vs.reserve(n);
	auto channel_offset = [&](int ch) {
		auto k0 = (float)ch     / data.num_channels;
		auto k1 = (float)(ch+1) / data.num_channels;
		auto lerp = [](float a, float b, float k) {
			return a*(1.0f - k) + b*k;
		};
		return lerp(1.0f, -1.0f, (k0 + k1) / 2);
	};
	float scale = data.zoom / data.num_channels;
	for (int ch = 0; ch < data.num_channels; ++ch) {
		for (int i = 0; i < n; ++i) {
			float x = 2.0f*i / (n - 1) - 1.0f;
			Vertex v = { x, channel_offset(ch) + scale * data.samples[i*data.num_channels + ch] };
			vs.emplace_back(std::move(v));
		}
	}

	auto target = _view->target();
	Float2 window_extents{(float)target._w, (float)target._h};
	int num_cb_elements = _window_extents_cb ? 2 : 0;
	create_or_update_buffer(_view->device,
							_view->context,
							std::vector<Float2>{window_extents, Float2{0, 0}},
							_window_extents_cb,
							num_cb_elements,
							D3D11_BIND_CONSTANT_BUFFER);
		
	auto ndc_to_window = [&](Float2 ndc) {
		return (ndc + 1.0f)/2.0f * window_extents;
	};
	float w = 3.0f;
	float r = 1.0f;
	std::vector<FPLInstance> instances;
	if (n > 0) {
		auto I = vs.data();
		instances.reserve((n - 1)*data.num_channels);
		for (int ch = 0; ch < data.num_channels; ++ch) {
			int num_lines = n-1;
			for (int i = 0; i < num_lines; ++i) {
				// http://http.developer.nvidia.com/GPUGems2/gpugems2_chapter22.html
				FPLInstance instance;
				auto x0 = I[0].x, y0 = I[0].y;
				auto x1 = I[1].x, y1 = I[1].y;
				Float2 p0 = instance.from = ndc_to_window(Float2{ x0, y0 });
				Float2 p1 = instance.to = ndc_to_window(Float2{ x1, y1 });
				instance.width = w;
				instance.r = r;
				instances.emplace_back(std::move(instance));
				++I;
			}
			++I;
		}
	}
	if (0) // debug line
	{
		FPLInstance instance;
		Float2 p0 = ndc_to_window({ -0.8f,  0.3f });
		Float2 p1 = ndc_to_window({  0.8f,  0.5f });
		instance.from = p0;
		instance.to = p1;
		float w = 15.0f;
		instance.width = w;
		instance.r = r;
		instances.emplace_back(std::move(instance));
	}
	create_or_update_buffer(_view->device, _view->context, instances, _fpl_instances, _num_fpl_instances);

	// update extents
	float const INCREMENT = 1.0f;
	std::vector<float> time_lines;
	auto L = std::floor(data.left_time / INCREMENT);
	auto R = std::ceil(data.right_time / INCREMENT);
	for (float t = L; t <= R; ++t) {
		auto biased = ((t * INCREMENT) - data.left_time) / (data.right_time - data.left_time);
		auto t_x = 2.0f * biased - 1.0f;
		time_lines.push_back(t_x);
	}
}

void SplineDrawer::update() {
	_timer.update();
	auto elapsed = _timer.elapsed();
	auto freq = (double)_timer.frequency();
	if ((elapsed - _last_resource_refresh) / freq > 0.5f) {
		load_refreshable_assets();
		_last_resource_refresh = elapsed;
	}
	double dt = (elapsed - _last_update) / freq;
	_last_update = elapsed;
#if SWEET_PARTICLES
	auto& last = _particles.front();
	auto& next = _particles.back();
	next.resize(0);
	for (auto p : last) {
		p.x = (float)(p.x + p.vx*dt);
		p.y = (float)(p.y + p.vy*dt);
		p.age += (float)dt;
		if (p.age < p.lifespan) {
			next.push_back(std::move(p));
		}
	}
	_particles.flip();
	create_or_update_buffer(_view->device,
							_view->context,
							_particles.front(),
							_particle_buffer,
							_num_particles);
#endif
}

void SplineDrawer::apply_stuff(CComPtr<ID3D11DeviceContext>& ctx, Stuff& stuff) {
	ctx->IASetPrimitiveTopology(stuff.primitive_topology);
	ctx->IASetInputLayout(stuff.input_layout);
	ctx->VSSetShader(stuff.vertex_shader, nullptr, 0);
	ctx->PSSetShader(stuff.pixel_shader, nullptr, 0);
}

void SplineDrawer::draw() {
	auto& ctx = _view->context;
	if (_fpl_instances) {
		apply_stuff(ctx, _fpl_stuff);

		CComPtr<ID3D11RenderTargetView> old_rtv;
		CComPtr<ID3D11DepthStencilView> old_dsv;
		ctx->OMGetRenderTargets(1, &old_rtv, &old_dsv);
		CComPtr<ID3D11DepthStencilState> old_dss;
		UINT old_stencil_ref;
		ctx->OMGetDepthStencilState(&old_dss, &old_stencil_ref);
		CComPtr<ID3D11BlendState> old_blend;
		float blend_factor[4] = {};
		UINT sample_mask = ~0;
		ctx->OMGetBlendState(&old_blend, blend_factor, &sample_mask);
		
		// draw
		auto& comp = _view->line_composition;
		float transparent_black[4] = {};
		ctx->ClearRenderTargetView(comp.render_target_view.p, transparent_black);
		ctx->OMSetRenderTargets(1, &comp.render_target_view.p, nullptr);
		UINT strides[] = { 2*sizeof(int16_t), sizeof(FPLInstance) };
		UINT offsets[] = { 0, 0 };
		ID3D11Buffer* buffers[] = { _fpl_vertex_ids.p, _fpl_instances.p };
		ctx->OMSetBlendState(_max_blend_state, blend_factor, ~0);
		ctx->VSSetConstantBuffers(0, 1, &_window_extents_cb.p);
		ctx->IASetVertexBuffers(0, 2, buffers, strides, offsets);
		ctx->IASetIndexBuffer(_fpl_indices, DXGI_FORMAT_R16_UINT, 0);
		ctx->DrawIndexedInstanced(4, _num_fpl_instances, 0, 0, 0);

		// compose
		ctx->OMSetRenderTargets(1, &old_rtv.p, old_dsv);
		apply_stuff(ctx, _line_compose_stuff);
		ctx->OMSetBlendState(_alpha_blend_state, blend_factor, sample_mask);
		ctx->PSSetShaderResources(0, 1, &comp.shader_resource_view.p);
		ctx->PSSetSamplers(0, 1, &_line_compose_sampler.p);
		ctx->IASetVertexBuffers(0, 1, &_fpl_vertex_ids.p, strides, offsets);
		ctx->IASetIndexBuffer(_fpl_indices, DXGI_FORMAT_R16_UINT, 0);
		ctx->Draw(3, 0);
		ID3D11ShaderResourceView* null_srvs[1] = {};
		ctx->PSSetShaderResources(0, 1, null_srvs);

		// reset
		ctx->OMSetBlendState(old_blend, blend_factor, sample_mask);
		ctx->OMSetDepthStencilState(old_dss, old_stencil_ref);
	}
#if SWEET_PARTICLES
	if (_particle_buffer) {
		apply_stuff(ctx, _particle_stuff);
		UINT stride = sizeof(Particle), offset = 0;
		ctx->IASetVertexBuffers(0, 1, &_particle_buffer.p, &stride, &offset);
		ctx->Draw(_num_particles, 0);
	}
#endif
}